<?php

namespace Application\Sonata\UserBundle\Tests\Entity;

/**
 * @file
 * @since  3.12.13 02:46 GMT+2
 * @author AJ Team <aj@team.tld>
 */

use Application\Sonata\UserBundle\Entity\User;
use PHPUnit_Framework_TestCase;

/**
 * Class UserTest
 *
 * @package Application\Sonata\UserBundle\Tests\Entity
 */
class UserTest extends PHPUnit_Framework_TestCase
{
    public function testSetters()
    {
        $entity = new User();

        $this->assertInstanceOf('Sonata\UserBundle\Entity\BaseUser', $entity);

        $this->assertNull($entity->getId());
    }
}
