<?php

namespace Application\Sonata\UserBundle\Tests\Entity;

/**
 * @file
 * @since  3.12.13 02:36 GMT+2
 * @author AJ Team <aj@team.tld>
 */

use Application\Sonata\UserBundle\Entity\Group;
use PHPUnit_Framework_TestCase;

/**
 * Class GroupTest
 *
 * @package Application\Sonata\UserBundle\Tests\Entity
 */
class GroupTest extends PHPUnit_Framework_TestCase
{
    public function testSetters()
    {
        $entity = new Group();

        $this->assertInstanceOf('Sonata\UserBundle\Entity\BaseGroup', $entity);

        $this->assertNull($entity->getId());
    }
}
