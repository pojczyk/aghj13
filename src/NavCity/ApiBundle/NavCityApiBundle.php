<?php

namespace NavCity\ApiBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class NavCityApiBundle extends Bundle
{
    protected $name = 'NavCityApiBundle';
}
