<?php

namespace NavCity\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use NavCity\ApiBundle\ORM\LatLng;
use Oh\GoogleMapFormTypeBundle\Validator\Constraints as OhAssert;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Point
 *
 * @ORM\Table(name="point", uniqueConstraints={@ORM\UniqueConstraint(name="lat_lng_UNIQUE", columns={"lat", "lng"})})
 * @ORM\Entity
 */
class Point
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false, unique=true)
     */
    private $name;

    /**
     * @var LatLng
     *
     * @ORM\Column(name="spacial_point", type="lat_lng")
     * @Exclude
     */
    private $spacialPoint;

    /**
     * @var float
     *
     * @ORM\Column(name="lat", type="decimal", precision=10, scale=8, nullable=false)
     */
    private $lat;

    /**
     * @var float
     *
     * @ORM\Column(name="lng", type="decimal", precision=11, scale=8, nullable=false)
     */
    private $lng;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=128, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Route", mappedBy="point")
     * @Exclude
     */
    private $route;

    /**
     * @var City
     *
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $city;

    /**
     * @var PointType
     *
     * @ORM\ManyToOne(targetEntity="PointType")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     * })
     */
    private $type;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->route = new ArrayCollection();
        $this->updateSpacialPoint();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Point
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param LatLng $spacialPoint
     *
     * @return $this
     */
    public function setSpacialPoint(LatLng $spacialPoint = null)
    {
        $this->spacialPoint = $spacialPoint;

        return $this;
    }

    /**
     * @return LatLng
     */
    public function getSpacialPoint()
    {
        return $this->spacialPoint;
    }

    /**
     * @return $this
     */
    private function updateSpacialPoint()
    {
        if (empty($this->lat) || empty($this->lng)) {
            return $this->setSpacialPoint(null);
        }

        return $this->setSpacialPoint(new LatLng($this->lat, $this->lng));
    }

    /**
     * Set lat
     *
     * @param float $lat
     *
     * @return Point
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        $this->updateSpacialPoint();

        return $this;
    }

    /**
     * Get lat
     *
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng
     *
     * @param float $lng
     *
     * @return Point
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        $this->updateSpacialPoint();

        return $this;
    }

    /**
     * Get lng
     *
     * @return float
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Point
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Point
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Point
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add route
     *
     * @param Route $route
     *
     * @return Point
     */
    public function addRoute(Route $route)
    {
        $this->route[] = $route;

        return $this;
    }

    /**
     * Remove route
     *
     * @param Route $route
     */
    public function removeRoute(Route $route)
    {
        $this->route->removeElement($route);
    }

    /**
     * Get route
     *
     * @return Collection
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set city
     *
     * @param City $city
     *
     * @return Point
     */
    public function setCity(City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set type
     *
     * @param PointType $type
     *
     * @return Point
     */
    public function setType(PointType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return PointType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param array $latlng
     *
     * @return $this
     */
    public function setLatLng(array $latlng)
    {
        $this->setLat($latlng['lat']);
        $this->setLng($latlng['lng']);

        return $this;
    }

    /**
     * @Assert\NotBlank()
     * @OhAssert\LatLng()
     */
    public function getLatLng()
    {
        return array('lat' => $this->getLat(), 'lng' => $this->getLng());
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return '' . $this->name;
    }

    /**
     * @param string $wrapWith
     *
     * @return string
     */
    public function getFormattedAddress($wrapWith = null)
    {
        $parts = array(
            $this->getAddress(),
            $this->getCity()->getName(),
            $this->getCity()->getCountry()->getName(),
        );

        if (!$parts = array_filter($parts)) {
            return null;
        }

        return sprintf('<%1$s>%2$s</%1$s>', $wrapWith ? : 'em', implode(', ', array_filter($parts)));
    }
}
