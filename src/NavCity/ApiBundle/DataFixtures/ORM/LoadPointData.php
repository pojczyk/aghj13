<?php

namespace NavCity\ApiBundle\DataFixtures\ORM;

/**
 * @file
 * @ingroup     Main
 * @brief       Brief
 *
 * More description..
 *
 * @since       7.06.13 11:01 GMT+2
 * @author      ... Team <...@team.tld>
 * @version     1
 * @date        7.06.13 11:01 GMT+2
 */
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NavCity\ApiBundle\Entity\Point;

/**
 * Class LoadPointData
 *
 * @package NavCity\ApiBundle\DataFixtures\ORM
 */
class LoadPointData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $counter = 0;
        // FIXME: More then one POI at single LAT/LNG
        $unique = array();
        $fp = fopen(__DIR__ . '/pois.csv', 'r');
        while ($row = fgetcsv($fp, null, ',', '"')) {
            $gps = $row[0] . $row[1];
            if (isset($unique[$gps])) {
                continue;
            }
            $unique[$gps] = true;

            $point = (new Point())
                ->setCity($this->getReference('city-krakow'))
                ->setLat($row[0])
                ->setLng($row[1])
                ->setName($row[2])
                ->setAddress($row[3])
                ->setType($this->getReference('point-type-poi'));
            $manager->persist($point);
            if (++$counter > 20) {
                $manager->flush();
                $manager->clear();
            }
        }
        fclose($fp);

        $manager->flush();
        $manager->clear();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3;
    }
}
