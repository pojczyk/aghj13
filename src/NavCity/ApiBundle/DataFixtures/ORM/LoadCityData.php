<?php

namespace NavCity\ApiBundle\DataFixtures\ORM;

/**
 * @file
 * @ingroup     Main
 * @brief       Brief
 *
 * More description..
 *
 * @since       7.06.13 10:50 GMT+2
 * @author      ... Team <...@team.tld>
 * @version     1
 * @date        7.06.13 10:50 GMT+2
 */

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NavCity\ApiBundle\Entity\City;

/**
 * Class LoadCityData
 *
 * @package NavCity\ApiBundle\DataFixtures\ORM
 */
class LoadCityData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $cities = array('krakow' => 'Kraków', 'rzeszow' => 'Rzeszów',);

        foreach ($cities as $slug => $name) {
            $city = new City();
            $city->setCountry($this->getReference('country-poland'));
            $city->setName($name);

            $manager->persist($city);
            $manager->flush();

            $this->addReference('city-' . $slug, $city);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2;
    }
}
