<?php

namespace NavCity\ApiBundle\DataFixtures\ORM;

/**
 * @file
 * @ingroup     Main
 * @brief       Brief
 *
 * More description..
 *
 * @since       7.06.13 10:57 GMT+2
 * @author      ... Team <...@team.tld>
 * @version     1
 * @date        7.06.13 10:57 GMT+2
 */

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NavCity\ApiBundle\Entity\PointType;

/**
 * Class LoadPointTypeData
 *
 * @package NavCity\ApiBundle\DataFixtures\ORM
 */
class LoadPointTypeData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $poi = new PointType();
        $poi->setName('POI');

        $event = new PointType();
        $event->setName('Wydarzenie');

        $manager->persist($poi);
        $manager->persist($event);
        $manager->flush();

        $this->addReference('point-type-poi', $poi);
        $this->addReference('point-type-event', $event);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}
