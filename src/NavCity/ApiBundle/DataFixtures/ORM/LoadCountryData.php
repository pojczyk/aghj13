<?php

namespace NavCity\ApiBundle\DataFixtures\ORM;

/**
 * @file
 * @ingroup     Main
 * @brief       Brief
 *
 * More description..
 *
 * @since       7.06.13 10:50 GMT+2
 * @author      ... Team <...@team.tld>
 * @version     1
 * @date        7.06.13 10:50 GMT+2
 */

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use NavCity\ApiBundle\Entity\Country;

/**
 * Class LoadCountryData
 *
 * @package NavCity\ApiBundle\DataFixtures\ORM
 */
class LoadCountryData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $country = new Country();
        $country->setName('Polska');

        $manager->persist($country);
        $manager->flush();

        $this->addReference('country-poland', $country);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}
