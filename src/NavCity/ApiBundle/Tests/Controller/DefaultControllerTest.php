<?php

namespace NavCity\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/hello/Marek');

        $this->assertTrue($crawler->filter('html:contains("Hello Marek")')->count() > 0);
    }
}
