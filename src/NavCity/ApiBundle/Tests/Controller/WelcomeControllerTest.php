<?php

namespace NavCity\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class WelcomeControllerTest extends WebTestCase
{
    public function testIndex()
    {
        // Create a new client to browse the application
        $client = static::createClient();

        // Browse POIs
        $crawler = $client->request('GET', '/');
        $this->assertSame(200, $client->getResponse()->getStatusCode(), 'Unexpected HTTP status code for GET /');
        $this->assertSame(1, $crawler->filter('html:contains("new google.maps.LatLng")')->count());

        $crawler = $client->request('GET', '/2');
        $this->assertSame(1, $crawler->filter('html:contains("new google.maps.LatLng")')->count());

        $crawler = $client->request('GET', '/1');
        $this->assertSame(1, $crawler->filter('html:contains("new google.maps.LatLng")')->count());
    }
}
