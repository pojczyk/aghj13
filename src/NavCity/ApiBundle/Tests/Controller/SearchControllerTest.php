<?php

namespace NavCity\ApiBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SearchControllerTest extends WebTestCase
{
    public function testIndex()
    {
        // Create a new client to browse the application
        $client = static::createClient();

        // Browse POIs
        $crawler = $client->request('GET', '/search/kraków');
        $this->assertSame(200, $client->getResponse()->getStatusCode(), 'Unexpected HTTP status code for GET /search/kraków');
        $this->assertSame(1, $crawler->filter('html:contains("new google.maps.LatLng")')->count());

        $form = $crawler->selectButton('Search')->form(array('term' => 'kraków',));
        $crawler = $client->submit($form);
        $this->assertSame(302, $client->getResponse()->getStatusCode(), 'Unexpected HTTP status code for GET /search-redirect');

        $crawler = $client->followRedirect();
        $this->assertSame(1, $crawler->filter('html:contains("new google.maps.LatLng")')->count());
    }
}
