<?php

namespace NavCity\ApiBundle\Filter;

use Assetic\Filter\LessphpFilter as BaseLessphpFilter;

/**
 * @file
 * @ingroup     Main
 * @brief       Brief
 *
 * More description..
 *
 * @since       11.05.13 00:45 GMT+2
 * @author      ... Team <...@team.tld>
 * @version     1
 * @date        11.05.13 00:45 GMT+2
 */

/**
 * Class LessphpFilter
 *
 * @package NavCity\ApiBundle\Filter
 */
class LessphpFilter extends BaseLessphpFilter
{
    public function setLoadPaths(array $loadPaths)
    {
        $this->loadPaths = $loadPaths;
    }
}
