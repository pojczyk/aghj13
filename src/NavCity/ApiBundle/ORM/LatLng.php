<?php

namespace NavCity\ApiBundle\ORM;

/**
 * LatLng object for spatial mapping
 */
class LatLng
{
    private $latitude;
    private $longitude;
    private $precision;

    public function __construct($latitude, $longitude, $precision = 8)
    {
        $this->setPrecision($precision);

        $this->setLatitude($latitude);
        $this->setLongitude($longitude);
    }

    public function setLatitude($latitude)
    {
        $this->latitude = $this->format($latitude);
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function setLongitude($longitude)
    {
        $this->longitude = $this->format($longitude);
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function setPrecision($precision)
    {
        $this->precision = (int) $precision;

        return $this;
    }

    public function getPrecision()
    {
        return $this->precision;
    }

    private function format($number)
    {
        return (float) number_format($number, $this->getPrecision(), '.', '');
    }

    public function __toString()
    {
        // Output from this is used with POINT_STR in DQL so must be in specific format
        return sprintf('POINT(%f %f)', $this->latitude, $this->longitude);
    }
}
