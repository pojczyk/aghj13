<?php

namespace NavCity\ApiBundle\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use Ivory\GoogleMap\Events\MouseEvent;
use Ivory\GoogleMap\Overlays\Marker;
use Ivory\GoogleMapBundle\Entity\Coordinate;
use Ivory\GoogleMapBundle\Entity\InfoWindow;
use Ivory\GoogleMapBundle\Entity\Map;
use NavCity\ApiBundle\Entity\Point;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

class WelcomeController extends Controller
{
    /**
     * @Route(path="/{limit}", requirements={"limit" = "\d+"}, defaults={"limit" = 20})
     * @Template()
     */
    public function indexAction($limit)
    {
        if ($limit < 2) {
            $limit = 2;
        }

        /** @var Map $map */
        $map = $this->get('ivory_google_map.map');
        // getPoints method returns iterator thus we have to access Point instance using $row[0]
        foreach ($this->getLatestPointWithNeighbours($limit) as $row) {
            $map->addMarker($this->createMarker($row[0]));
        }

        return array('map' => $map,);
    }

    /**
     * @param Point $point
     *
     * @return Marker
     */
    private function createMarker(Point $point)
    {
        $coordinate = new Coordinate($point->getLat(), $point->getLng());
        if ($address = $point->getFormattedAddress()) {
            $address = sprintf('<p>%s</p>', $address);
        }
        if ($description = $point->getDescription()) {
            $description = sprintf('<p>%s</p>', $description);
        }
        $content    = sprintf('<h3>%s</h3>%s%s', $point->getName(), $address, $description);
        $infoWindow = new InfoWindow($content, $coordinate, null, false, MouseEvent::CLICK, true, true);

        return new Marker($coordinate, null, null, null, null, $infoWindow);
    }

    /**
     * @param integer $limit
     *
     * @return array|IterableResult
     */
    private function getLatestPointWithNeighbours($limit)
    {
        $latestPoint = $this->getDoctrine()->getRepository('NavCityApiBundle:Point')
            ->findBy(array(), array('id' => 'DESC',), 1);

        if (empty($latestPoint)) {
            return array();
        }

        /** @var Point $latestPoint */
        $latestPoint = current($latestPoint);

        /** @var EntityManager $em */
        $em    = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
            ->select('p', 'DISTANCE(p.spacialPoint, POINT_STR(:spacial)) AS HIDDEN dist')
            ->from('NavCityApiBundle:Point', 'p')
            ->innerJoin('NavCityApiBundle:PointType', 'pt')
            ->where('pt.name = :type')
            ->orderBy('dist', 'ASC')
            ->setParameter('type', 'POI')
            ->setParameter('spacial', $latestPoint->getSpacialPoint())
            ->setMaxResults($limit)
            ->getQuery();

        return $query->iterate();
    }
}
