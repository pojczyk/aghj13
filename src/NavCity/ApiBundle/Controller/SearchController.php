<?php

namespace NavCity\ApiBundle\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use Ivory\GoogleMap\Events\MouseEvent;
use Ivory\GoogleMap\Overlays\Marker;
use Ivory\GoogleMapBundle\Entity\Coordinate;
use Ivory\GoogleMapBundle\Entity\InfoWindow;
use Ivory\GoogleMapBundle\Entity\Map;
use NavCity\ApiBundle\Entity\Point;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

class SearchController extends Controller
{
    /**
     * @Route("/search-redirect")
     * @Method("POST")
     */
    public function formAction()
    {
        $parameters = array('term' => $this->getRequest()->request->get('term'), 'limit' => 20,);
        return $this->redirect($this->generateUrl('navcity_api_search_index', $parameters));
    }

    /**
     * @Route(path="/search/{term}/{limit}", requirements={"limit" = "\d+"}, defaults={"limit" = 20})
     * @Method("GET")
     * @Template()
     */
    public function indexAction($term, $limit)
    {
        if ($limit < 2) {
            $limit = 2;
        }

        /** @var Map $map */
        $map = $this->get('ivory_google_map.map');
        foreach ($this->getMatchingPoints($term, $limit) as $row) {
            $map->addMarker($this->createMarker($row[0]));
        }

        return compact('term', 'map');
    }

    /**
     * @param Point $point
     *
     * @return Marker
     */
    private function createMarker(Point $point)
    {
        $coordinate = new Coordinate($point->getLat(), $point->getLng());
        if ($address = $point->getFormattedAddress()) {
            $address = sprintf('<p>%s</p>', $address);
        }
        if ($description = $point->getDescription()) {
            $description = sprintf('<p>%s</p>', $description);
        }
        $content    = sprintf('<h3>%s</h3>%s%s', $point->getName(), $address, $description);
        $infoWindow = new InfoWindow($content, $coordinate, null, false, MouseEvent::CLICK, true, true);

        return new Marker($coordinate, null, null, null, null, $infoWindow);
    }

    /**
     * @param string  $term
     * @param integer $limit
     *
     * @return array|IterableResult
     */
    private function getMatchingPoints($term, $limit)
    {
        $term = trim($term);

        if (empty($term)) {
            return array();
        }

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('NavCityApiBundle:Point', 'p')
            ->where('p.name LIKE :term')
            ->orWhere('p.address LIKE :term')
            ->setParameter('term', '%' . $term . '%')
            ->setMaxResults($limit)
            ->getQuery();

        return $query->iterate();
    }
}
