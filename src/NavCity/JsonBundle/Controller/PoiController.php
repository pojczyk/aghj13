<?php

namespace NavCity\JsonBundle\Controller;

use NavCity\ApiBundle\Entity\Point;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class PoiController extends Controller
{
    /**
     * @Route(path="/poi/{id}", requirements={"id" = "\d+"})
     * @Template()
     */
    public function indexAction($id)
    {
        return array('poi' => $this->findPoi($id),);
    }

    /**
     * @Route("/pois")
     * @Template()
     */
    public function listAction()
    {
        return array('pois' => $this->findPois(),);
    }

    /**
     * @param integer $id
     *
     * @return array|null
     */
    private function findPoi($id)
    {
        /** @var Point $poi */
        if (!$poi = $this->getDoctrine()->getRepository('NavCityApiBundle:Point')->find($id)) {
            return null;
        }

        return array(
            'id'      => $poi->getId(),
            'name'    => $poi->getName() ? : '',
            'content' => $poi->getDescription() ? : '',
        );
    }

    /**
     * @return array
     */
    private function findPois()
    {
        $pois = array();
        /** @var Point $poi */
        foreach ($this->getDoctrine()->getRepository('NavCityApiBundle:Point')->findAll() as $poi) {
            $pois[] = array(
                'id'   => $poi->getId(),
                'name' => $poi->getName(),
                'lat'  => $poi->getLat(),
                'lng'  => $poi->getLng(),
            );
        }

        return $pois;
    }
}
