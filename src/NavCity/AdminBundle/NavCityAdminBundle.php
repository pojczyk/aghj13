<?php

namespace NavCity\AdminBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class NavCityAdminBundle extends Bundle
{
    protected $name = 'NavCityAdminBundle';
}
