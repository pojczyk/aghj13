<?php

namespace NavCity\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

/**
 * @file
 * @ingroup     Main
 * @brief       Brief
 *
 * More description..
 *
 * @since       11.05.13 10:21 GMT+2
 * @author      ... Team <...@team.tld>
 * @version     1
 * @date        11.05.13 10:21 GMT+2
 */

/**
 * Class RouteAdmin
 *
 * @package NavCity\AdminBundle\Admin
 */
class RouteAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name')
            ->add('city')
            ->add('user')
            ->add('point', 'sonata_type_model', array('expanded' => true, 'by_reference' => false, 'multiple' => true))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
        ;
    }
}
