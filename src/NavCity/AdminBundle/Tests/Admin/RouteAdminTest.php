<?php

namespace NavCity\AdminBundle\Tests\Admin;

/**
 * @file
 * @since  3.12.13 02:52 GMT+2
 * @author AJ Team <aj@team.tld>
 */

use PHPUnit_Framework_TestCase;
use ReflectionClass;

/**
 * Class RouteAdminTest
 *
 * @package NavCity\AdminBundle\Tests\Admin
 */
class RouteAdminTest extends PHPUnit_Framework_TestCase
{
    private $admin;

    public function setUp()
    {
        $this->admin = $this->getMock('NavCity\AdminBundle\Admin\RouteAdmin', array(), array(), '', false);
    }

    public function testConfigureFormFields()
    {
        $class      = 'Sonata\AdminBundle\Form\FormMapper';
        $formMapper = $this->getMock($class, array('add'), array(), '', false);
        $formMapper->expects($this->exactly(4))->method('add')->will($this->returnSelf());
        $this->getMethod('configureFormFields')->invoke($this->admin, $formMapper);
    }

    public function testConfigureDatagridFilters()
    {
        $class    = 'Sonata\AdminBundle\Datagrid\DatagridMapper';
        $argument = $this->getMock($class, array('add'), array(), '', false);
        $argument->expects($this->exactly(1))->method('add')->will($this->returnSelf());
        $this->getMethod('configureDatagridFilters')->invoke($this->admin, $argument);
    }

    public function testConfigureListFields()
    {
        $class    = 'Sonata\AdminBundle\Datagrid\ListMapper';
        $argument = $this->getMock($class, array('addIdentifier'), array(), '', false);
        $argument->expects($this->exactly(1))->method('addIdentifier')->will($this->returnSelf());
        $this->getMethod('configureListFields')->invoke($this->admin, $argument);
    }

    private function getMethod($name)
    {
        $class  = new ReflectionClass(get_class($this->admin));
        $method = $class->getMethod($name);
        $method->setAccessible(true);

        return $method;
    }
}
