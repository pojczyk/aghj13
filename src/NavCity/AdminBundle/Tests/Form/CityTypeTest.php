<?php

namespace NavCity\AdminBundle\Tests\Form;

/**
 * @file
 * @since  3.12.13 03:29 GMT+2
 * @author AJ Team <aj@team.tld>
 */

use NavCity\AdminBundle\Form\CityType;
use PHPUnit_Framework_TestCase;

/**
 * Class CityTypeTest
 *
 * @package NavCity\AdminBundle\Tests\Form
 */
class CityTypeTest extends PHPUnit_Framework_TestCase
{
    /** @var CityType */
    private $type;

    public function setUp()
    {
        $this->type = new CityType();
    }

    public function testBuildForm()
    {
        $fb = $this->getMock('Symfony\Component\Form\FormBuilder', array('add'), array(), '', false);
        $fb->expects($this->exactly(2))->method('add')->will($this->returnSelf());

        $this->type->buildForm($fb, array());
    }

    public function testGetName()
    {
        $this->assertSame('navcity_adminbundle_citytype', $this->type->getName());
    }
}
