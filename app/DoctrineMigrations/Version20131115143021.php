<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20131115143021 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");

        // DROP TABLE
        $this->addSql("DROP TABLE point_near");

        // DROP FOREIGN KEY
        $this->addSql("ALTER TABLE city DROP FOREIGN KEY fk_city_country1");
        $this->addSql("ALTER TABLE event DROP FOREIGN KEY fk_event_point1");
        $this->addSql("ALTER TABLE log_record DROP FOREIGN KEY fk_log_severity_log1");
        $this->addSql("ALTER TABLE point DROP FOREIGN KEY fk_point_city1");
        $this->addSql("ALTER TABLE point DROP FOREIGN KEY fk_point_point_type1");
        $this->addSql("ALTER TABLE route DROP FOREIGN KEY fk_route_city");
        $this->addSql("ALTER TABLE route DROP FOREIGN KEY fk_route_user");
        $this->addSql("ALTER TABLE route_point DROP FOREIGN KEY fk_route_point_route1");
        $this->addSql("ALTER TABLE route_point DROP FOREIGN KEY fk_route_point_point1");

        // DROP KEY
        $this->addSql("DROP INDEX starts_at_ends_at_idx ON event");
        $this->addSql("DROP INDEX name_idx ON event");
        $this->addSql("DROP INDEX created_at_idx ON log_record");
        $this->addSql("DROP INDEX name_UNIQUE ON route");
        $this->addSql("DROP INDEX uid_UNIQUE ON user");

        // DROP KEY + CHANGE/ADD/DROP FIELD
        $this->addSql("ALTER TABLE point DROP INDEX name_UNIQUE, ADD UNIQUE INDEX UNIQ_B7A5F3245E237E06 (name)");

        // CHANGE/ADD/DROP FIELD
        $this->addSql("ALTER TABLE country CHANGE id id INT AUTO_INCREMENT NOT NULL");
        $this->addSql("ALTER TABLE city CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE country_id country_id INT NOT NULL");
        $this->addSql("ALTER TABLE event CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE point_id point_id INT NOT NULL");
        $this->addSql("ALTER TABLE log_record CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE severity_id severity_id INT NOT NULL");
        $this->addSql("ALTER TABLE log_severity CHANGE id id INT AUTO_INCREMENT NOT NULL");
        $this->addSql("ALTER TABLE point ADD spacial_point POINT NOT NULL, CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE city_id city_id INT NOT NULL, CHANGE type_id type_id INT DEFAULT NULL");
        $this->addSql("ALTER TABLE point_type CHANGE id id INT AUTO_INCREMENT NOT NULL");
        $this->addSql("ALTER TABLE route CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE city_id city_id INT NOT NULL, CHANGE user_id user_id INT DEFAULT NULL");
        $this->addSql("ALTER TABLE route_point CHANGE route_id route_id INT NOT NULL, CHANGE point_id point_id INT NOT NULL");
        $this->addSql("ALTER TABLE user ADD name VARCHAR(255) NOT NULL, ADD roles LONGTEXT NOT NULL COMMENT '(DC2Type:array)', DROP uid, CHANGE id id INT AUTO_INCREMENT NOT NULL");

        // CREATE TABLE
        $this->addSql("CREATE TABLE nc_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(255) NOT NULL, username_canonical VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, email_canonical VARCHAR(255) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, locked TINYINT(1) NOT NULL, expired TINYINT(1) NOT NULL, expires_at DATETIME DEFAULT NULL, confirmation_token VARCHAR(255) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT '(DC2Type:array)', credentials_expired TINYINT(1) NOT NULL, credentials_expire_at DATETIME DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, date_of_birth DATETIME DEFAULT NULL, firstname VARCHAR(64) DEFAULT NULL, lastname VARCHAR(64) DEFAULT NULL, website VARCHAR(64) DEFAULT NULL, biography VARCHAR(255) DEFAULT NULL, gender VARCHAR(1) DEFAULT NULL, locale VARCHAR(8) DEFAULT NULL, timezone VARCHAR(64) DEFAULT NULL, phone VARCHAR(64) DEFAULT NULL, facebook_uid VARCHAR(255) DEFAULT NULL, facebook_name VARCHAR(255) DEFAULT NULL, facebook_data LONGTEXT DEFAULT NULL COMMENT '(DC2Type:json)', twitter_uid VARCHAR(255) DEFAULT NULL, twitter_name VARCHAR(255) DEFAULT NULL, twitter_data LONGTEXT DEFAULT NULL COMMENT '(DC2Type:json)', gplus_uid VARCHAR(255) DEFAULT NULL, gplus_name VARCHAR(255) DEFAULT NULL, gplus_data LONGTEXT DEFAULT NULL COMMENT '(DC2Type:json)', token VARCHAR(255) DEFAULT NULL, two_step_code VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_DE11C47D92FC23A8 (username_canonical), UNIQUE INDEX UNIQ_DE11C47DA0D96FBF (email_canonical), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("CREATE TABLE fos_user_user_group (user_id INT NOT NULL, group_id INT NOT NULL, INDEX IDX_B3C77447A76ED395 (user_id), INDEX IDX_B3C77447FE54D947 (group_id), PRIMARY KEY(user_id, group_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("CREATE TABLE acl_classes (id INT UNSIGNED AUTO_INCREMENT NOT NULL, class_type VARCHAR(200) NOT NULL, UNIQUE INDEX UNIQ_69DD750638A36066 (class_type), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("CREATE TABLE acl_security_identities (id INT UNSIGNED AUTO_INCREMENT NOT NULL, identifier VARCHAR(200) NOT NULL, username TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8835EE78772E836AF85E0677 (identifier, username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("CREATE TABLE acl_object_identities (id INT UNSIGNED AUTO_INCREMENT NOT NULL, parent_object_identity_id INT UNSIGNED DEFAULT NULL, class_id INT UNSIGNED NOT NULL, object_identifier VARCHAR(100) NOT NULL, entries_inheriting TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_9407E5494B12AD6EA000B10 (object_identifier, class_id), INDEX IDX_9407E54977FA751A (parent_object_identity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("CREATE TABLE acl_object_identity_ancestors (object_identity_id INT UNSIGNED NOT NULL, ancestor_id INT UNSIGNED NOT NULL, INDEX IDX_825DE2993D9AB4A6 (object_identity_id), INDEX IDX_825DE299C671CEA1 (ancestor_id), PRIMARY KEY(object_identity_id, ancestor_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
        $this->addSql("CREATE TABLE acl_entries (id INT UNSIGNED AUTO_INCREMENT NOT NULL, class_id INT UNSIGNED NOT NULL, object_identity_id INT UNSIGNED DEFAULT NULL, security_identity_id INT UNSIGNED NOT NULL, field_name VARCHAR(50) DEFAULT NULL, ace_order SMALLINT UNSIGNED NOT NULL, mask INT NOT NULL, granting TINYINT(1) NOT NULL, granting_strategy VARCHAR(30) NOT NULL, audit_success TINYINT(1) NOT NULL, audit_failure TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4 (class_id, object_identity_id, field_name, ace_order), INDEX IDX_46C8B806EA000B103D9AB4A6DF9183C9 (class_id, object_identity_id, security_identity_id), INDEX IDX_46C8B806EA000B10 (class_id), INDEX IDX_46C8B8063D9AB4A6 (object_identity_id), INDEX IDX_46C8B806DF9183C9 (security_identity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");

        // ADD CONSTRAINT
        $this->addSql("ALTER TABLE city ADD CONSTRAINT FK_2D5B0234F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)");
        $this->addSql("ALTER TABLE log_record ADD CONSTRAINT FK_8ECECC33F7527401 FOREIGN KEY (severity_id) REFERENCES log_severity (id)");
        $this->addSql("ALTER TABLE fos_user_user_group ADD CONSTRAINT FK_B3C77447A76ED395 FOREIGN KEY (user_id) REFERENCES nc_user (id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE fos_user_user_group ADD CONSTRAINT FK_B3C77447FE54D947 FOREIGN KEY (group_id) REFERENCES user (id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE acl_object_identities ADD CONSTRAINT FK_9407E54977FA751A FOREIGN KEY (parent_object_identity_id) REFERENCES acl_object_identities (id)");
        $this->addSql("ALTER TABLE acl_object_identity_ancestors ADD CONSTRAINT FK_825DE2993D9AB4A6 FOREIGN KEY (object_identity_id) REFERENCES acl_object_identities (id) ON UPDATE CASCADE ON DELETE CASCADE");
        $this->addSql("ALTER TABLE acl_object_identity_ancestors ADD CONSTRAINT FK_825DE299C671CEA1 FOREIGN KEY (ancestor_id) REFERENCES acl_object_identities (id) ON UPDATE CASCADE ON DELETE CASCADE");
        $this->addSql("ALTER TABLE acl_entries ADD CONSTRAINT FK_46C8B806EA000B10 FOREIGN KEY (class_id) REFERENCES acl_classes (id) ON UPDATE CASCADE ON DELETE CASCADE");
        $this->addSql("ALTER TABLE acl_entries ADD CONSTRAINT FK_46C8B8063D9AB4A6 FOREIGN KEY (object_identity_id) REFERENCES acl_object_identities (id) ON UPDATE CASCADE ON DELETE CASCADE");
        $this->addSql("ALTER TABLE acl_entries ADD CONSTRAINT FK_46C8B806DF9183C9 FOREIGN KEY (security_identity_id) REFERENCES acl_security_identities (id) ON UPDATE CASCADE ON DELETE CASCADE");
        $this->addSql("ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA7C028CEA2 FOREIGN KEY (point_id) REFERENCES point (id)");
        $this->addSql("ALTER TABLE point ADD CONSTRAINT FK_B7A5F3248BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)");
        $this->addSql("ALTER TABLE point ADD CONSTRAINT FK_B7A5F324C54C8C93 FOREIGN KEY (type_id) REFERENCES point_type (id)");
        $this->addSql("ALTER TABLE route ADD CONSTRAINT FK_2C420798BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)");
        $this->addSql("ALTER TABLE route ADD CONSTRAINT FK_2C42079A76ED395 FOREIGN KEY (user_id) REFERENCES nc_user (id)");
        $this->addSql("ALTER TABLE route_point ADD CONSTRAINT FK_2ADAC18A34ECB4E6 FOREIGN KEY (route_id) REFERENCES route (id)");
        $this->addSql("ALTER TABLE route_point ADD CONSTRAINT FK_2ADAC18AC028CEA2 FOREIGN KEY (point_id) REFERENCES point (id)");

        // CREATE KEY
        $this->addSql("CREATE UNIQUE INDEX UNIQ_2C420795E237E06 ON route (name)");
        $this->addSql("CREATE UNIQUE INDEX UNIQ_8D93D6495E237E06 ON user (name)");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");

        // DROP TABLE
        $this->addSql("DROP TABLE nc_user");
        $this->addSql("DROP TABLE fos_user_user_group");
        $this->addSql("DROP TABLE acl_classes");
        $this->addSql("DROP TABLE acl_security_identities");
        $this->addSql("DROP TABLE acl_object_identities");
        $this->addSql("DROP TABLE acl_object_identity_ancestors");
        $this->addSql("DROP TABLE acl_entries");

        // DROP FOREIGN KEY
        $this->addSql("ALTER TABLE city DROP FOREIGN KEY FK_2D5B0234F92F3E70");
        $this->addSql("ALTER TABLE log_record DROP FOREIGN KEY FK_8ECECC33F7527401");
        $this->addSql("ALTER TABLE route DROP FOREIGN KEY FK_2C42079A76ED395");
        $this->addSql("ALTER TABLE fos_user_user_group DROP FOREIGN KEY FK_B3C77447A76ED395");
        $this->addSql("ALTER TABLE acl_entries DROP FOREIGN KEY FK_46C8B806EA000B10");
        $this->addSql("ALTER TABLE acl_entries DROP FOREIGN KEY FK_46C8B806DF9183C9");
        $this->addSql("ALTER TABLE acl_object_identities DROP FOREIGN KEY FK_9407E54977FA751A");
        $this->addSql("ALTER TABLE acl_object_identity_ancestors DROP FOREIGN KEY FK_825DE2993D9AB4A6");
        $this->addSql("ALTER TABLE acl_object_identity_ancestors DROP FOREIGN KEY FK_825DE299C671CEA1");
        $this->addSql("ALTER TABLE acl_entries DROP FOREIGN KEY FK_46C8B8063D9AB4A6");
        $this->addSql("ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA7C028CEA2");
        $this->addSql("ALTER TABLE point DROP FOREIGN KEY FK_B7A5F3248BAC62AF");
        $this->addSql("ALTER TABLE point DROP FOREIGN KEY FK_B7A5F324C54C8C93");
        $this->addSql("ALTER TABLE route_point DROP FOREIGN KEY FK_2ADAC18A34ECB4E6");
        $this->addSql("ALTER TABLE route_point DROP FOREIGN KEY FK_2ADAC18AC028CEA2");

        // DROP KEY
        $this->addSql("DROP INDEX UNIQ_2C420795E237E06 ON route");
        $this->addSql("DROP INDEX UNIQ_8D93D6495E237E06 ON user");

        // DROP KEY + CHANGE/ADD/DROP FIELD
        $this->addSql("ALTER TABLE point DROP INDEX UNIQ_B7A5F3245E237E06, ADD INDEX name_UNIQUE (name)");
        $this->addSql("ALTER TABLE point DROP spacial_point, CHANGE id id INT UNSIGNED AUTO_INCREMENT NOT NULL, CHANGE city_id city_id INT UNSIGNED NOT NULL, CHANGE type_id type_id INT UNSIGNED DEFAULT NULL");
        $this->addSql("ALTER TABLE user ADD uid VARCHAR(48) NOT NULL, DROP name, DROP roles, CHANGE id id INT UNSIGNED AUTO_INCREMENT NOT NULL");

        // CHANGE/ADD/DROP FIELD
        $this->addSql("ALTER TABLE city CHANGE id id INT UNSIGNED AUTO_INCREMENT NOT NULL, CHANGE country_id country_id INT UNSIGNED NOT NULL");
        $this->addSql("ALTER TABLE country CHANGE id id INT UNSIGNED NOT NULL");
        $this->addSql("ALTER TABLE event CHANGE id id INT UNSIGNED NOT NULL, CHANGE point_id point_id INT UNSIGNED NOT NULL");
        $this->addSql("ALTER TABLE log_record CHANGE id id INT UNSIGNED AUTO_INCREMENT NOT NULL, CHANGE severity_id severity_id INT UNSIGNED NOT NULL");
        $this->addSql("ALTER TABLE log_severity CHANGE id id INT UNSIGNED NOT NULL");
        $this->addSql("ALTER TABLE point_type CHANGE id id INT UNSIGNED AUTO_INCREMENT NOT NULL");
        $this->addSql("ALTER TABLE route CHANGE id id INT UNSIGNED AUTO_INCREMENT NOT NULL, CHANGE city_id city_id INT UNSIGNED NOT NULL, CHANGE user_id user_id INT UNSIGNED DEFAULT NULL");
        $this->addSql("ALTER TABLE route_point CHANGE route_id route_id INT UNSIGNED NOT NULL, CHANGE point_id point_id INT UNSIGNED NOT NULL");

        // CREATE TABLE
        $this->addSql("CREATE TABLE point_near (near_point_id INT UNSIGNED NOT NULL, point_id INT UNSIGNED NOT NULL, INDEX fk_point_near_point1_idx (point_id), INDEX fk_point_near_point2_idx (near_point_id), PRIMARY KEY(near_point_id, point_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");

        // ADD CONSTRAINT
        $this->addSql("ALTER TABLE point_near ADD CONSTRAINT fk_point_near_point1 FOREIGN KEY (point_id) REFERENCES point (id) ON UPDATE CASCADE ON DELETE CASCADE");
        $this->addSql("ALTER TABLE point_near ADD CONSTRAINT fk_point_near_point2 FOREIGN KEY (near_point_id) REFERENCES point (id) ON UPDATE NO ACTION ON DELETE NO ACTION");
        $this->addSql("ALTER TABLE event ADD CONSTRAINT fk_event_point1 FOREIGN KEY (point_id) REFERENCES point (id) ON UPDATE CASCADE");
        $this->addSql("ALTER TABLE point ADD CONSTRAINT fk_point_city1 FOREIGN KEY (city_id) REFERENCES city (id) ON UPDATE CASCADE");
        $this->addSql("ALTER TABLE point ADD CONSTRAINT fk_point_point_type1 FOREIGN KEY (type_id) REFERENCES point_type (id) ON UPDATE CASCADE ON DELETE CASCADE");
        $this->addSql("ALTER TABLE route ADD CONSTRAINT fk_route_city FOREIGN KEY (city_id) REFERENCES city (id) ON UPDATE CASCADE");
        $this->addSql("ALTER TABLE route ADD CONSTRAINT fk_route_user FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE CASCADE");
        $this->addSql("ALTER TABLE route_point ADD CONSTRAINT fk_route_point_route1 FOREIGN KEY (point_id) REFERENCES point (id) ON UPDATE CASCADE ON DELETE CASCADE");
        $this->addSql("ALTER TABLE route_point ADD CONSTRAINT fk_route_point_point1 FOREIGN KEY (route_id) REFERENCES route (id) ON UPDATE CASCADE ON DELETE CASCADE");

        // CREATE KEY
        $this->addSql("CREATE UNIQUE INDEX name_UNIQUE ON route (name, user_id, city_id)");
        $this->addSql("CREATE UNIQUE INDEX uid_UNIQUE ON user (uid)");
        $this->addSql("CREATE INDEX starts_at_ends_at_idx ON event (starts_at, ends_at)");
        $this->addSql("CREATE INDEX name_idx ON event (name)");
        $this->addSql("CREATE INDEX created_at_idx ON log_record (created_at)");
    }
}
