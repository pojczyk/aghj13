SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `navcity` ;
CREATE SCHEMA IF NOT EXISTS `navcity` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
SHOW WARNINGS;
USE `navcity` ;

-- -----------------------------------------------------
-- Table `navcity`.`country`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `navcity`.`country` (
  `id` INT UNSIGNED NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `navcity`.`city`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `navcity`.`city` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `country_id` INT UNSIGNED NOT NULL ,
  `name` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) ,
  INDEX `fk_city_country1_idx` (`country_id` ASC) ,
  CONSTRAINT `fk_city_country1`
    FOREIGN KEY (`country_id` )
    REFERENCES `navcity`.`country` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `navcity`.`user`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `navcity`.`user` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `uid` VARCHAR(48) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `uid_UNIQUE` (`uid` ASC) )
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `navcity`.`route`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `navcity`.`route` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `city_id` INT UNSIGNED NOT NULL ,
  `user_id` INT UNSIGNED NULL ,
  `name` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC, `user_id` ASC, `city_id` ASC) ,
  INDEX `fk_route_city_idx` (`city_id` ASC) ,
  INDEX `fk_route_user_idx` (`user_id` ASC) ,
  CONSTRAINT `fk_route_city`
    FOREIGN KEY (`city_id` )
    REFERENCES `navcity`.`city` (`id` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_route_user`
    FOREIGN KEY (`user_id` )
    REFERENCES `navcity`.`user` (`id` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `navcity`.`point_type`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `navcity`.`point_type` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `navcity`.`point`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `navcity`.`point` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `type_id` INT UNSIGNED NULL ,
  `city_id` INT UNSIGNED NOT NULL ,
  `name` VARCHAR(255) NOT NULL ,
  `lat` DECIMAL(10,8) NOT NULL ,
  `lng` DECIMAL(11,8) NOT NULL ,
  `image` VARCHAR(255) NULL ,
  `address` VARCHAR(128) NULL ,
  `description` TEXT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_point_city1_idx` (`city_id` ASC) ,
  INDEX `fk_point_point_type1_idx` (`type_id` ASC) ,
  INDEX `name_UNIQUE` (`name` ASC) ,
  UNIQUE INDEX `lat_lng_UNIQUE` (`lat` ASC, `lng` ASC) ,
  CONSTRAINT `fk_point_city1`
    FOREIGN KEY (`city_id` )
    REFERENCES `navcity`.`city` (`id` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_point_point_type1`
    FOREIGN KEY (`type_id` )
    REFERENCES `navcity`.`point_type` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `navcity`.`route_point`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `navcity`.`route_point` (
  `route_id` INT UNSIGNED NOT NULL ,
  `point_id` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`route_id`, `point_id`) ,
  INDEX `fk_route_point_route1_idx` (`route_id` ASC) ,
  INDEX `fk_route_point_point1_idx` (`route_id` ASC) ,
  CONSTRAINT `fk_route_point_route1`
    FOREIGN KEY (`point_id` )
    REFERENCES `navcity`.`point` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_route_point_point1`
    FOREIGN KEY (`route_id` )
    REFERENCES `navcity`.`route` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `navcity`.`point_near`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `navcity`.`point_near` (
  `point_id` INT UNSIGNED NOT NULL ,
  `near_point_id` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`near_point_id`, `point_id`) ,
  INDEX `fk_point_near_point1_idx` (`point_id` ASC) ,
  INDEX `fk_point_near_point2_idx` (`near_point_id` ASC) ,
  CONSTRAINT `fk_point_near_point1`
    FOREIGN KEY (`point_id` )
    REFERENCES `navcity`.`point` (`id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_point_near_point2`
    FOREIGN KEY (`near_point_id` )
    REFERENCES `navcity`.`point` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `navcity`.`log_severity`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `navcity`.`log_severity` (
  `id` INT UNSIGNED NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `navcity`.`log_record`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `navcity`.`log_record` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `severity_id` INT UNSIGNED NOT NULL ,
  `message` TEXT NOT NULL ,
  `created_at` DATETIME NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `created_at_idx` (`created_at` ASC) ,
  INDEX `fk_log_severity_log1_idx` (`severity_id` ASC) ,
  CONSTRAINT `fk_log_severity_log1`
    FOREIGN KEY (`severity_id` )
    REFERENCES `navcity`.`log_severity` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `navcity`.`event`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `navcity`.`event` (
  `id` INT UNSIGNED NOT NULL ,
  `point_id` INT UNSIGNED NOT NULL ,
  `name` VARCHAR(255) NOT NULL ,
  `starts_at` DATETIME NULL ,
  `ends_at` DATETIME NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_event_point1_idx` (`point_id` ASC) ,
  INDEX `starts_at_ends_at_idx` (`starts_at` ASC, `ends_at` ASC) ,
  INDEX `name_idx` (`name` ASC) ,
  CONSTRAINT `fk_event_point1`
    FOREIGN KEY (`point_id` )
    REFERENCES `navcity`.`point` (`id` )
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;
USE `navcity` ;

-- -----------------------------------------------------
-- Placeholder table for view `navcity`.`available_country`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `navcity`.`available_country` (`id` INT, `name` INT);
SHOW WARNINGS;

-- -----------------------------------------------------
-- Placeholder table for view `navcity`.`available_city`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `navcity`.`available_city` (`id` INT, `country_id` INT, `name` INT);
SHOW WARNINGS;

-- -----------------------------------------------------
-- Placeholder table for view `navcity`.`log`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `navcity`.`log` (`id` INT, `severity_id` INT, `message` INT, `created_at` INT, `severity` INT);
SHOW WARNINGS;

-- -----------------------------------------------------
-- Placeholder table for view `navcity`.`near_poi`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `navcity`.`near_poi` (`id` INT);
SHOW WARNINGS;

-- -----------------------------------------------------
-- Placeholder table for view `navcity`.`near_point`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `navcity`.`near_point` (`id` INT, `type_id` INT, `city_id` INT, `name` INT, `lat` INT, `lng` INT, `image` INT, `address` INT, `description` INT, `near_this_id` INT);
SHOW WARNINGS;

-- -----------------------------------------------------
-- Placeholder table for view `navcity`.`near_event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `navcity`.`near_event` (`event_name` INT, `starts_at` INT, `ends_at` INT);
SHOW WARNINGS;

-- -----------------------------------------------------
-- Placeholder table for view `navcity`.`near_current_event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `navcity`.`near_current_event` (`id` INT);
SHOW WARNINGS;

-- -----------------------------------------------------
-- View `navcity`.`available_country`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `navcity`.`available_country`;
SHOW WARNINGS;
USE `navcity`;
CREATE  OR REPLACE VIEW `navcity`.`available_country` AS
SELECT `c`.*
  FROM `country` `c`
 WHERE EXISTS(SELECT * FROM `available_city` `ac` WHERE `ac`.`country_id` = `c`.`id`);
SHOW WARNINGS;

-- -----------------------------------------------------
-- View `navcity`.`available_city`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `navcity`.`available_city`;
SHOW WARNINGS;
USE `navcity`;
CREATE  OR REPLACE VIEW `navcity`.`available_city` AS
SELECT `c`.*
  FROM `city` `c`
 WHERE EXISTS(SELECT * FROM `point` `p` WHERE `p`.`city_id` = `c`.`id`)
;
SHOW WARNINGS;

-- -----------------------------------------------------
-- View `navcity`.`log`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `navcity`.`log`;
SHOW WARNINGS;
USE `navcity`;
CREATE  OR REPLACE VIEW `navcity`.`log` AS
SELECT `lr`.*, `ls`.`name` `severity`
  FROM `log_record` `lr`
		LEFT JOIN `log_severity` `ls` ON `ls`.`id` = `lr`.`severity_id`
;
SHOW WARNINGS;

-- -----------------------------------------------------
-- View `navcity`.`near_poi`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `navcity`.`near_poi`;
SHOW WARNINGS;
USE `navcity`;
CREATE  OR REPLACE VIEW `navcity`.`near_poi` AS
SELECT `np`.*
  FROM `near_point` `np`
        INNER JOIN `point_type` `pt` ON `pt`.`id` = `np`.`type_id` AND `pt`.`name` = 'poi'
;
SHOW WARNINGS;

-- -----------------------------------------------------
-- View `navcity`.`near_point`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `navcity`.`near_point`;
SHOW WARNINGS;
USE `navcity`;
CREATE  OR REPLACE VIEW `navcity`.`near_point` AS
SELECT `np`.*, `p`.`id` `near_this_id`
  FROM `point` `p`
		INNER JOIN `point_near` `pn` ON `pn`.`point_id` = `p`.`id`
		INNER JOIN `point` `np` ON `np`.`id` = `pn`.`near_point_id`
;
SHOW WARNINGS;

-- -----------------------------------------------------
-- View `navcity`.`near_event`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `navcity`.`near_event`;
SHOW WARNINGS;
USE `navcity`;
CREATE  OR REPLACE VIEW `navcity`.`near_event` AS
SELECT `np`.*, `e`.`name` `event_name`, `e`.`starts_at`, `e`.`ends_at`
  FROM `near_point` `np`
		INNER JOIN `event` `e` ON `e`.`point_id` = `np`.`id` AND (`e`.`ends_at` IS NULL OR DATEDIFF(`e`.`ends_at`, NOW()) >= 0)

;
SHOW WARNINGS;

-- -----------------------------------------------------
-- View `navcity`.`near_current_event`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `navcity`.`near_current_event`;
SHOW WARNINGS;
USE `navcity`;
CREATE  OR REPLACE VIEW `navcity`.`near_current_event` AS
SELECT *
  FROM `near_event`
 WHERE `starts_at` IS NULL OR DATEDIFF(`starts_at`, NOW()) BETWEEN -1 AND 1
;
SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
